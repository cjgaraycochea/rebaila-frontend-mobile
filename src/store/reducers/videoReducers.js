// Global state used for "videos"

const initialState = {
  videos: null,
};

export default function videoConstructor(state = initialState, action) {
  switch (action.type) {
    case 'SET_REDUCER_VIDEOS':
      return {
        ...state,
        videos: action.videos
      };
    default:
      return state;
  }
}