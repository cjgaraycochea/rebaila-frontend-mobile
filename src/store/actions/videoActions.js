/* Functions for set "video" in reducers */

export const setReducerVideos = (data) => async dispatch => {
  dispatch({
    type: 'SET_REDUCER_VIDEOS',
    videos: data
  })
}