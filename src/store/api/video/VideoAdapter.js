/* Functions for convert "video" response from API to local objects  */

export function getVideosAdapter(response) {
  const items = response.data.Items.map((video)=> {
    const { 
      video_id,
      video_title,
      video_description,
      video_tag_level,
      video_gender,
      video_video,
      video_thumbnail,
      video_time
    } = video;
    return {
      id: video_id,
      title: video_title,
      description: video_description,
      tag_level: video_tag_level,
      gender: video_gender,
      video: video_video,
      thumbnail: video_thumbnail,
      time: video_time
    }
  })
  return items;
}