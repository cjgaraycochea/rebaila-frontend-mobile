/* Functions for get, set, remove and update data from API related to endpoints of "video" */

import axios from 'axios';
import { API } from '../../config/constants';
import { getVideosAdapter } from './VideoAdapter';

export async function getApiVideos() {
  return await axios.get(`${API}/get_videos`)
    .then((response) => {
      const data = getVideosAdapter(response);
      return data;
    })
    .catch((error) => {
      console.log(error);
      return null;
    });
};

