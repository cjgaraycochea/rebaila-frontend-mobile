import * as React from 'react';
import { View } from 'react-native';
import { Video } from 'expo-av';

export default function Player(props: any) {
  const { route } = props;
  const { videoSrc } =route.params;
  const video = React.useRef(null);
  const [status, setStatus] = React.useState({});

  return (
    <View>
      <Video
        ref={video}
        style={{width:'100%', height: '100%'}}
        source={{
          uri: videoSrc,
        }}
        useNativeControls
        resizeMode="contain"
        isLooping
        onPlaybackStatusUpdate={status => setStatus(() => status)}
      />
    </View>
  );
}
